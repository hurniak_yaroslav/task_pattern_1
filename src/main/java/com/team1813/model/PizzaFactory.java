package com.team1813.model;

import com.team1813.model.pizzaImp.CheesePizza;
import com.team1813.model.pizzaImp.ClamPizza;
import com.team1813.model.pizzaImp.PepperoniPizza;
import com.team1813.model.pizzaImp.VeggiePizza;

public class PizzaFactory {

    public Pizza createPizza(PizzaType type) {
        switch (type) {
            case CHEESE:
                return new CheesePizza();
            case VEGGIE:
                return new VeggiePizza();
            case CLAM:
                return new ClamPizza();
            case PEPPERONI:
                return new PepperoniPizza();
            default:
                throw new RuntimeException("Can`t found Pizza type to create.");
        }
    }

}
