package com.team1813.model;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}
