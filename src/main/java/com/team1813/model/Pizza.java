package com.team1813.model;

public interface Pizza {
    void prepare();

    void bake();

    void cut();

    void box();
}
