package com.team1813.model.pizzaImp;

import com.team1813.model.Pizza;

public class VeggiePizza implements Pizza {
    @Override
    public void prepare() {

    }

    @Override
    public void bake() {

    }

    @Override
    public void cut() {

    }

    @Override
    public void box() {

    }
}
